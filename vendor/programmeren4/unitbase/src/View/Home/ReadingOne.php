<!-- Use case for Inserting -->
<main>
    <header>
        <h1>SelectOne</h1>
    </header>
    <article>
        <nav>
            <a href=""></a>
            <a href=""></a>
        </nav>
        <form action="/unitbase.php/Home/Del" method="post">
                 <div><label for="UnitBaseCode">Code: </label>
                <?php echo $model->getCode();?></div>
                
                <div> <label for="UnitBaseName">Naam: </label>
                <?php echo $model->getName();?></div>
                
                <div> <label for="UnitBaseDescription">Beschrijving: </label>
                <?php echo $model->getDescription();?></div>
                
                 <div><label for="UnitBaseShippingCostMultiplier">Verzendkostenfactor: </label>
                <?php echo $model->getShippingCostMultiplier();?></div>
                
                 <a href="/unitbase.php/Home/Updating/<?php echo $model->getId();?>">Update</a>
                 <a href="/unitbase.php/Home/Deleting/<?php echo $model->getId();?>">Delete</a>
                
        </form>
        
    </article>
</main
<?php $appStateView(); ?>
