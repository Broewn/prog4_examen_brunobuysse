<!-- Use case for Inserting -->
<main>
    <header>
        <h1>Update</h1>
    </header>
    <article>
        <nav>
            <a href=""></a>
            <a href=""></a>
        </nav>
        <form action="/unitbase.php/Home/Update" method="post">
            <input name="ArticleId" type="text" value="<?php echo $model->getId();?>"/>
            <!-- (div>label+input)*3 -->
            <div>
                <label for="UnitBaseCode">Code</label>
                <input type="text" name="UnitBaseCode" value="<?php echo $model->getCode();?>"></div>
            <div>
                <label for="UnitBaseName">Naam</label>
                <input type="text" name="UnitBaseName" value="<?php echo $model->getName();?>"></div>
            <div>
                <label for="UnitBaseDescription">Beschrijving</label>
                <input type="text" name="UnitBaseDescription" value="<?php echo $model->getDescription();?>"></div>
            <div>
                <label for="UnitBaseShippingCostMultiplier">Verzendkostenfactor</label>
                <input type="text" name="UnitBaseShippingCostMultiplier" value="<?php echo $model->getShippingCostMultiplier();?>"></div>
            <button type="submit">Update</button>
        </form>
    </article>
</main
<?php $appStateView(); ?>
