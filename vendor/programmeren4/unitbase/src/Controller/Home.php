<?php
namespace programmeren4\unitbase\Controller;

class Home extends \ModernWays\Mvc\Controller
{
    private $pdo;
    private $model;
    
    public function __construct(\ModernWays\Mvc\Route $route = null, \ModernWays\Dialog\Model\INoticeBoard $noticeBoard = null){
        parent::__construct($route, $noticeBoard);
        $this->noticeBoard->startTimeInKey('PDO Connection');
        try {
            $this->pdo = new \PDO('mysql:host=localhost;dbname=MikMak;charset=utf8', 'broewn', '');
            $this->noticeBoard->setText('PDO connectie gelukt!');
            $this->noticeBoard->setCaption('PDO connectie voor UnitBase');
        } catch (\Exception $e) {
            $this->noticeBoard->setText("{$e->getMessage()} op lijn {$e->getLine()} in bestand {$e->getFile()}");
            $this->noticeBoard->setCaption('PDO connectie voor UnitBase');
            $this->noticeBoard->setCode($e->getCode());
        }
        $this->noticeBoard->log(); 
        // create een instantie van een prikbord om validatiefouten
        // op te plaatsen
        $modelState = new \ModernWays\Dialog\Model\NoticeBoard();
        $this->model = new \Programmeren4\Unitbase\Model\Unitbase($modelState);
    } 
    
    
    public function editing()
    {
        if($this->pdo){
            // het model vullen
            $command = $this->pdo->query("call UnitBaseSelectAll");
            // associatieve array kolomnamen en waarde per rij
            $this->model->setList($command->fetchAll(\PDO::FETCH_ASSOC));
            return $this->view('Home','Editing', $this->model);
        }
        else return $this->view('Home', 'Error', null);
    }
    
    public function inserting(){
        return $this->view('Home', 'Inserting', null);
    }
    public function insert() {
        if(!$this->pdo) {
            return $this->view('Home', 'Error');
        }
        // steekt de invulvelden in het model
        $this->model->setName(filter_input(INPUT_POST, 'UnitBaseName', FILTER_SANITIZE_STRING));
        $this->model->setDescription(filter_input(INPUT_POST, 'UnitBaseDescription', FILTER_SANITIZE_STRING));
        $this->model->setCode(filter_input(INPUT_POST, 'UnitBaseCode', FILTER_SANITIZE_STRING));
        $this->model->setShippingCostMultiplier(filter_input(INPUT_POST, 'UnitBaseShippingCostMultiplier', FILTER_SANITIZE_FLOAT));
        // controlleert ofdat alles aan de eisen voldoet en schrijft het naar de database
        if($this->model->isValid()) {
            $statement = $this->pdo->prepare("Call UnitBaseInsert(:pName, :pCode, :pDescription, :pShippingCostMultiplier, @pId)");
            // steekt de waarden van de invulvelden in een model en stuurt ze meet met de stored procedure.
            
            $statement->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
            $statement->bindValue(':pCode', $this->model->getCode(), \PDO::PARAM_STR);
            $statement->bindValue(':pDescription', $this->model->getDescription(), \PDO::PARAM_STR);
            $statement->bindValue(':pShippingCostMultiplier', $this->model->getShippingCostMultiplier(), \PDO::PARAM_STR);
            
            $statement->execute();
            
            $this->model->setId($this->pdo->query('SELECT @pId')->fetchColumn());
            
            
            return $this->view('Home', 'Editing', $this->model);
            
        } else {
            return $this->view('Home', 'Inserting', $this->model);
        }
    }
    
    public function readingone() {
        // derde parameter in het pad, is meestal een id
        if ($this->pdo){
            $this->model->setId($this->route->getId());
            $statement = $this->pdo->prepare("call UnitBaseSelectOne(:pId)"); 
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $result = $statement->execute();
            $articleOne = $statement->fetch(\PDO::FETCH_ASSOC);
            $this->model->setName($articleOne['Name']);
            $this->model->setCode($articleOne['Code']);
            $this->model->setDescription($articleOne['Description']);
            $this->model->setShippingCostMultiplier($articleOne['ShippingCostMultiplier']);
            return $this->view('Home','ReadingOne', $this->model);
        }
        else{
            return $this->view('Home', 'Error', null);
        }
    }
    
        public function updating() {
        // derde parameter in het pad, is meestal een id
        if ($this->pdo){
            $this->model->setId($this->route->getId());
            $statement = $this->pdo->prepare("call UnitBaseSelectOne(:pId)"); 
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $result = $statement->execute();
            $articleOne = $statement->fetch(\PDO::FETCH_ASSOC);
            $this->model->setName($articleOne['Name']);
            $this->model->setCode($articleOne['Code']);
            $this->model->setDescription($articleOne['Description']);
            $this->model->setShippingCostMultiplier($articleOne['ShippingCostMultiplier']);
            return $this->view('Home','Updating', $this->model);
        }
        else{
            return $this->view('Home', 'Error', null);
        }
    }
    
    public function update(){
           if(!$this->pdo) {
            return $this->view('Home', 'Error');
        }
        $this->model->setName(filter_input(INPUT_POST, 'UnitBaseName', FILTER_SANITIZE_STRING));
        $this->model->setCode(filter_input(INPUT_POST, 'UnitBaseCode', FILTER_SANITIZE_STRING));
        $this->model->setDescription(filter_input(INPUT_POST, 'UnitBaseDescription', FILTER_SANITIZE_STRING));
        $this->model->setShippingCostMultiplier(filter_input(INPUT_POST, 'UnitBaseShippingCostMultiplier', FILTER_SANITIZE_NUMBER_FLOAT));
        
        if($this->model->isValid()) {
            $sth = $this->pdo->prepare("CALL UnitBaseUpdate(:pName, :pCode, :pDescription, :pShippingCostMultiplier, :pId)");
            $sth->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
            $sth->bindValue(':pCode', $this->model->getCode(), \PDO::PARAM_STR);
            $sth->bindValue(':pDescription', $this->model->getDescription(), \PDO::PARAM_STR);
            $sth->bindValue(':pShippingCostMultiplier', $this->model->getShippingCostMultiplier(), \PDO::PARAM_INT);
            $sth->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $sth->execute();
            
            /**
             * Ik gebruik hier header() omdat ik niet alleen de editing pagina te zien wil krijgen,
             * ik wil effectief doorgestuurd worden naar deze pagina. 
             */
            return $this->view('Home', 'Editing', $this->model);
            //return $this->editing();
        } else {
            return $this->view('Home', 'Updating', $this->model);
        }
    }
    
    public function deleting() {
       
        if($this->pdo) {
            // zoek op basis van het id, om vervolgens het te verwijderen artikel weer te geven
            $this->model->setId($this->route->getId());
            $statement = $this->pdo->prepare("Call UnitBaseSelectOne(:pId)");
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $statement->execute();
            $array = $statement->fetch(\PDO::FETCH_ASSOC);
            
            $this->model->setName($array['Name']);
            $this->model->setCode($array['Code']);
            $this->model->setDescription($array['Description']);
            $this->model->setShippingCostMultiplier($array['ShippingCostMultiplier']);
    
            return $this->view('Home', 'Deleting', $this->model);
        } else {
            return $this->view('Home', 'Error');
        }
    }
   
   // artikel verwijderen
   public function del() {
        if(!$this->pdo) {
            return $this->view('Home', 'Error');
        }
        $this->model->setId(filter_input(INPUT_POST, 'UnitBaseId', FILTER_SANITIZE_NUMBER_INT));
        if($this->pdo) {
            $statement = $this->pdo->prepare("Call UnitBaseDelete(:pId)");
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $statement->execute();
            
            /**
             * Ik gebruik hier header() omdat ik niet alleen de editing pagina te zien wil krijgen,
             * ik wil effectief doorgestuurd worden naar deze pagina. 
             */
            return $this->view('Home', 'Editing', $this->model);
            //return $this->editing();
        } else {
            return $this->view('Home', 'Editing', $this->model);
        }
    }

}