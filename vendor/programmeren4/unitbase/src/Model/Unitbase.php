<?php

namespace Programmeren4\Unitbase\Model;
class Unitbase extends \ModernWays\Mvc\Model
{
    private $id;
    private $name;
    private $code;
    private $description;
    private $shippingCostMultiplier;
    
    /**
    * @return mixed
    */
    public function getShippingCostMultiplier()
    {
        return $this->shippingCostMultiplier;
    }
    
    public function setShippingCostMultiplier($shippingCostMultiplier)
    {
        $this->shippingCostMultiplier = $shippingCostMultiplier;
    } 
    
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }   
    
        /**
    * @return mixed
    */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
        /**
    * @return mixed
    */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }  
    
    /**
    * @return mixed
    */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }  
    
    /**
    * @return mixed
    */

    
}